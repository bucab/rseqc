"""
Setup script for RSeQC -- Comprehensive QC package for RNA-seq data
"""
from __future__ import print_function

import sys
import os
import platform
import glob
from distutils.core import setup
from setuptools import *


def main():
    setup(name="RSeQC",
          version=open('VERSION').read().strip(),
          packages=find_packages('lib'),
          package_dir={'': 'lib'},
          package_data={'': ['*.ps']},
          scripts=glob.glob("scripts/*.py"),
          ext_modules=[],
          py_modules=['psyco_full'],
          test_suite='nose.collector',
          setup_requires=['nose>=0.10.4', 'cython>=0.12'],
          author="Liguo Wang, Adam Labadorf",
          author_email="wangliguo78@gmail.com,labadorf@bu.edu",
          platforms=['Linux', 'MacOS'],
          requires=['cython (>=0.17)'],
          install_requires=['cython>=0.17', 'pysam', 'bx-python', 'numpy'],
          description="RNA-seq QC Package",
          long_description=open('README.md').read().strip(),
          url="http://rseqc.sourceforge.net/",
          zip_safe=False,
          dependency_links=[],
          classifiers=[
              'Development Status :: 5 - Production/Stable',
              'Environment :: Console',
              'Intended Audience :: Science/Research',
              'License :: OSI Approved :: GNU General Public License v2 (GPLv2)',
              'Operating System :: MacOS :: MacOS X',
              'Operating System :: POSIX',
              'Programming Language :: Python',
              'Topic :: Scientific/Engineering :: Bio-Informatics',
          ],
          keywords='RNA-seq, QC',
          )


if __name__ == "__main__":
    main()
