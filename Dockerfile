FROM python:3.6-stretch

RUN rm -rf /var/lib/apt/lists* && \
    apt update -y && \
    apt install -y git

# rseqc updated to python 3
RUN apt install liblzo2-dev
RUN pip3 install numpy setuptools future
RUN git clone https://bitbucket.org/bubioinformaticshub/rseqc
RUN (cd rseqc; python3.6 setup.py install)

WORKDIR "/usr/local/bin/"
